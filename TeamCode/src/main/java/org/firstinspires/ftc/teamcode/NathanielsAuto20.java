package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by Robotics Student on 12/1/2016.
 */

@Autonomous(name = "Nathaniels 20", group = "Nathaniel")
public class NathanielsAuto20 extends LinearOpMode {

	private void sleep_seconds(double sec) {
		ElapsedTime runtime = new ElapsedTime();
		runtime.reset();
		while(opModeIsActive() && runtime.seconds() < sec) {
			idle();
		}
	}

	@Override
	public void runOpMode() throws InterruptedException {
		NathanielsRobot robot = new NathanielsRobot(hardwareMap, this, true);
		waitForStart();
		sleep_seconds(20);
		robot.auto_drive(48.5f);
	}
}
