package org.firstinspires.ftc.teamcode;

import android.media.AudioManager;
import android.media.SoundPool;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Telemetry;

/**
 * Created by Robotics Student on 12/1/2016.
 */

@TeleOp(name = "Nathaniels Teleop", group = "Nathaniel")
public class NathanielsTeleop extends LinearOpMode {
	private float scale(float v) {
		return Math.signum((Math.abs(v) > 0.1)?v:0) * v * v;
	}

	@Override
	public void runOpMode() throws InterruptedException {
		NathanielsRobot robot = new NathanielsRobot(hardwareMap, this, false);
		waitForStart();
		while(opModeIsActive()) {
			robot.setDrive(scale(-gamepad1.left_stick_y), scale(-gamepad1.right_stick_y));
			if(gamepad1.a) {
				robot.setLift(128);
			} else if(gamepad1.x && gamepad1.b && gamepad1.y) {
				robot.setLift(-60);
			} else {
				robot.setLift(0);
			}

			robot.updateTelemetry(telemetry);
		}
	}
}
