package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.Telemetry;

import java.util.Locale;


class NathanielsRobot {
	protected Telemetry telemetry;
	private LinearOpMode opMode;
	private boolean autonomous;

	private DcMotor drive_L;
	private DcMotor drive_R;

	private DcMotor lift_L;
	private DcMotor lift_R;

	private final float ENCODER_CPR = 1120;
	private final float WHEEL_DIAMETER = 4;
	private final float GEAR_RATIO = 2;
	private final float AXLE_LENGTH = 13;

	NathanielsRobot(HardwareMap hardwareMap, LinearOpMode opMode_, boolean autonomous_) {
		opMode = opMode_;
		autonomous = autonomous_;

		drive_L = hardwareMap.dcMotor.get("motorLeft");
		drive_L.setDirection(DcMotorSimple.Direction.FORWARD);

		drive_R = hardwareMap.dcMotor.get("motorRight");
		drive_R.setDirection(DcMotorSimple.Direction.REVERSE);

		lift_L = hardwareMap.dcMotor.get("liftLeft");
		lift_L.setDirection(DcMotorSimple.Direction.FORWARD);

		lift_R = hardwareMap.dcMotor.get("liftRight");
		lift_R.setDirection(DcMotorSimple.Direction.REVERSE);

		if(autonomous) {
			drive_L.setMode(DcMotor.RunMode.RUN_TO_POSITION);
			drive_R.setMode(DcMotor.RunMode.RUN_TO_POSITION);
			drive_L.setPower(0.5);
			drive_R.setPower(0.5);
		} else {
			drive_L.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
			drive_R.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
		}
	}

	void updateTelemetry(Telemetry telemetry) {
		telemetry.addData("Drive L", String.format(Locale.US, "<% 4.2f, % d>", drive_L.getPower(), drive_L.getCurrentPosition()));
		telemetry.addData("Drive R", String.format(Locale.US, "<% 4.2f, % d>", drive_R.getPower(), drive_R.getCurrentPosition()));
		telemetry.addData("Lift <L, R>", String.format(Locale.US, "<% 4.2f, % 4.2f>", lift_L.getPower(), lift_R.getPower()));
		telemetry.update();
	}

	void setDrive(float l, float r) {
		if(!autonomous) {
			drive_L.setPower(l);
			drive_R.setPower(r);
		}
	}

	void setLift(float p) {
		lift_L.setPower(p);
		lift_R.setPower(p);
	}

	void auto_rotate(float angle) {
		float inches = AXLE_LENGTH * (angle/2);
		int encoder = (int) ((inches/(Math.PI * WHEEL_DIAMETER)) * GEAR_RATIO * ENCODER_CPR);

		if(autonomous) {
			drive_L.setTargetPosition(drive_L.getCurrentPosition() + encoder);
			drive_R.setTargetPosition(drive_R.getCurrentPosition() - encoder);
			drive_L.setPower(.5);
			drive_R.setPower(.5);
			while((drive_L.isBusy() || drive_R.isBusy())  && opMode.opModeIsActive()) {
				opMode.idle();
			}
			drive_L.setPower(0);
			drive_R.setPower(0);
		}
	}

	void auto_drive(float inches) {
		int encoder = (int) ((inches/(Math.PI * WHEEL_DIAMETER)) * GEAR_RATIO * ENCODER_CPR);

		if(autonomous) {
			drive_L.setTargetPosition(drive_L.getCurrentPosition() + encoder);
			drive_R.setTargetPosition(drive_R.getCurrentPosition() + encoder);
			drive_L.setPower(.5);
			drive_R.setPower(.5);
			while((drive_L.isBusy() || drive_R.isBusy()) && opMode.opModeIsActive()) {
				opMode.idle();
			}
			drive_L.setPower(0);
			drive_R.setPower(0);
		}
	}
}
